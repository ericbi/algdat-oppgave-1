"""
TASK:
You are given an array of numbers that tell the change in a stock's price from day to day.
Create a function that finds the best days to buy and sell the stock to maximise profits 
(that means, find the maximum positive difference between buyprice and sellprice). Selling
the stock must happen after you have bought the stock.
An example of the given array of numbers:
    
    stock_changes = [-1, 3, -9, 2, 2, -1, 2, -1, -5]

In the example it is best to buy after the fall on day 3, and sell on after the rise on day 7.
"""


stock_changes = [-1, 3, -9, 2, 2, -1, 2, -1, -5]

def task1(stock_changes):
	"""Find the biggest buy-sell interval profit given an array of the changes in a stock.

	Algorithm:
		Time Complexity - O(n)
		Method          - Loop through the array of changes and keep track of the running
		                  sum (the price of the stock up until index i). If the running sum
		                  is at a minimum at i, set that to be the new buy point (it's best
		                  to buy at the dip and sell at the highest price after that).
		                  If there lies a point after that at which the profit is higher
		                  than any profit we've previously encounted, we set this to be
		                  the new best profit potential!
	"""
	stock_value = 0

	lowest_value = 0
	lowest_value_index = 0

	highest_value = 0
	highest_value_index = 0

	for i, change in enumerate(stock_changes):
		# The profit if we were to sell now, at index i
		current_profit = stock_value - lowest_value
		best_profit = highest_value - lowest_value

		# We're at the lowest point
		if stock_value < lowest_value:
			lowest_value_index = i
			lowest_value = stock_value

			# We need to reset the highest value when the lowest value is found.
			# Otherwise the "highest value" might be a point BEFORE the lowest value
			# resulting in an answer like (3, 2) where the price dropped drastically...
			highest_value = stock_value

		# If we had sold now, the profit would be better than the previous best profit we found
		elif current_profit > best_profit:
			highest_value = stock_value
			highest_value_index = i

		# Change the stock value
		stock_value += change

	return (lowest_value_index, highest_value_index)


# Print the solution
print(task1(stock_changes))


import numpy as np
import timeit

runs = 10**5

timings = []

for n in range(1, 20, 2):
	print(f"Timing O({n})", end="\r")

	time_taken_in_seconds = timeit.timeit(lambda: task1(stock_changes*n), number=runs)
	microseconds = time_taken_in_seconds / runs * 1000**2
	
	print(f"Timing O({n}) - Took {microseconds:.2f}us per run [{time_taken_in_seconds:.3f}s total]")

	timings.append((n, microseconds))



# Plot the results as a graph to see if the trend is approx. O(n), O(n^2), O(n*log(n)) etc...
import matplotlib.pyplot as plt
plt.plot(*zip(*timings))
plt.show()